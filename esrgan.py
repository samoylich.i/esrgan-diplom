from uuid import uuid4
import cv2
import numpy as np
import torch


def super_resolution(image, model):
    img = image * 1.0 / 255
    img_LR = torch.from_numpy(np.transpose(img[:, :, [2, 1, 0]], (2, 0, 1))).float()
    img_LR = img_LR.unsqueeze(0)
    img_LR = img_LR.cpu()

    with torch.no_grad():
        output = model(img_LR).data.squeeze().float().cpu().clamp_(0, 1).numpy()
    output = np.transpose(output[[2, 1, 0], :, :], (1, 2, 0))
    output = (output * 255.0).round()
    new_filepath = 'results/{:s}_rlt.png'.format(uuid4().hex)
    cv2.imwrite(new_filepath, output)
    return new_filepath
