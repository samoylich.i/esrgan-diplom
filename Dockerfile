FROM anibali/pytorch:cuda-10.0

COPY ./ .

RUN pip install flask numpy opencv-python

CMD ["python3", "app.py"]