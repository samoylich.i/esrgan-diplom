
import flask
from flask import request, Flask, send_file, render_template, jsonify
import cv2
import numpy as np
import torch
import RRDBNet_arch as arch
from esrgan import super_resolution
import config

app = Flask(__name__)
app.config.from_object(config)

@app.route('/sr', methods=['POST', 'GET'])
def sr():
    if request.method == 'POST':
        img = request.files['file'].read()
        img = np.fromstring(img, np.uint8)
        img = cv2.imdecode(img, flags=1)
        result_filename = super_resolution(image=img, model=model)
        return send_file(result_filename)
    else:
        img_path = None
        result = []
    return render_template('upload.html', img_path=img_path, result=result)

if __name__ == '__main__':
    print("Loading PyTorch model and Flask starting server ...")
    print("Please wait until server has fully started")
    gpu_id = None
    model_path = 'models/RRDB_ESRGAN_x4.pth'
    device = torch.device('cpu')
    model = arch.RRDBNet(3, 3, 64, 23, gc=32)
    model.load_state_dict(torch.load(model_path), strict=True)
    model.eval()
    model = model.to(device)
    app.run()